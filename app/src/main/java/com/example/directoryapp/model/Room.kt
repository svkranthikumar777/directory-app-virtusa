package com.example.directoryapp.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Room(

	@Json(name="isOccupied")
	val isOccupied: Boolean? = null,

	@Json(name="createdAt")
	val createdAt: String? = null,

	@Json(name="maxOccupancy")
	val maxOccupancy: Int? = null,

	@Json(name="id")
	val id: String? = null
):BaseModel()
