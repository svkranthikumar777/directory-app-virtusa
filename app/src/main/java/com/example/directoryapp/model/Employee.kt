package com.example.directoryapp.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Employee(

	@Json(name="createdAt")
	val createdAt: String? = null,

	@Json(name="firstName")
	val firstName: String? = null,

	@Json(name="lastName")
	val lastName: String? = null,

	@Json(name="jobtitle")
	val jobtitle: String? = null,

	@Json(name="avatar")
	val avatar: String? = null,

	@Json(name="id")
	val id: String? = null,

	@Json(name="email")
	val email: String? = null,

	@Json(name="favouriteColor")
	val favouriteColor: String? = null
):BaseModel()
