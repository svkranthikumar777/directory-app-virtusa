package com.example.directoryapp.retrofit

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class RequestInterceptor: Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!NetworkCheck.instance.isOnline()) {
            throw OfflineException()
        }

        return chain.proceed(chain.request().newBuilder().build())
    }

    class OfflineException : IOException() {
        val errorMessage: String
            get() = "No network available, please check your WiFi or Data connection"
    }
}