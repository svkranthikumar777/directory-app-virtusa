package com.example.directoryapp.retrofit

import com.example.directoryapp.utils.GlobalVariables
import java.io.IOException
import java.net.UnknownHostException

object ErrorResponseHandler {

    fun parseException(error: Throwable) {
        try {
            error.printStackTrace()

            if (error is RequestInterceptor.OfflineException) {
                GlobalVariables.snackBarMessage.postValue(RequestInterceptor.OfflineException().errorMessage)
                //this is an actual network failure :( inform the user and possibly retry")
            } else  {
                GlobalVariables.snackBarMessage.postValue(error.localizedMessage)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}