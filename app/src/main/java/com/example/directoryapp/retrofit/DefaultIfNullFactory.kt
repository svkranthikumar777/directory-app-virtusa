package com.example.directoryapp.retrofit

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import java.lang.reflect.Type

// This class will handle api response json parsing to model class and checks for possible data type issues for the keys that defined in model class
class DefaultIfNullFactory : JsonAdapter.Factory {
    override fun create(
        type: Type, annotations: MutableSet<out Annotation>,
        moshi: Moshi
    ): JsonAdapter<*> {
        val delegate = moshi.nextAdapter<Any>(this, type, annotations)
        return object : JsonAdapter<Any>() {
            override fun fromJson(reader: JsonReader): Any? {
                val blob1 = reader.readJsonValue()
                return try {
                    val blob = blob1 as Map<*, *>
                    val noNulls = blob.filterValues { it != null }
                    delegate.fromJsonValue(noNulls)
                } catch (e: Exception) {
                    try {
                        delegate.fromJsonValue(blob1)
                    } catch (e: Exception) {
                        // assign null value for a key if different data type received for any key in model class
                        // this will avoid the response type getting converted to kotlin Unit class in case of different response data types received from api
                        null
                    }
                }
            }

            override fun toJson(writer: JsonWriter, value: Any?) {
                return delegate.toJson(writer, value)
            }
        }
    }
}