package com.example.directoryapp.retrofit

import okhttp3.logging.HttpLoggingInterceptor

class LoggingInterceptor {
    fun loggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }
}