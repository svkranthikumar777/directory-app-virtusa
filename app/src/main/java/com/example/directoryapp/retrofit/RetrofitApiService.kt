package com.example.directoryapp.retrofit

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import com.example.directoryapp.model.Employee
import com.example.directoryapp.model.Room
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import java.security.KeyStore
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager
import javax.security.cert.CertificateException

object RetrofitApiService {

    //API 19 SUPPORT
//    private fun getKitkatSupportSSL(): ArrayList<CipherSuite> {
//        val cipherSuites = arrayListOf<CipherSuite>(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
//
//        ConnectionSpec.MODERN_TLS.cipherSuites()?.run {
//            if (!contains(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)) {
//                cipherSuites.add(CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA);
//            }
//        }
//
//        return cipherSuites
//    }
//
//    private var spec: ConnectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//        .cipherSuites(getKitkatSupportSSL().toTypedArray()[0])
//        .build()

    @SuppressLint("CustomX509TrustManager", "TrustAllX509TrustManager")
    fun getUnsafeOkHttpClient(builder: OkHttpClient.Builder?): OkHttpClient.Builder {
        try {
            // Create a trust manager that does not validate certificate chains
            /*val trustAllCerts = arrayOf<TrustManager>(
                object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                    }

                    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                        return arrayOf()
                    }
                })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory*/

            if (builder == null) OkHttpClient.Builder()
//            builder?.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder?.hostnameVerifier { _, _ -> true }

            return builder!!
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    private fun getOkHTTPClient(): OkHttpClient {
//        val client = OkHttpClient.Builder()
//            .connectTimeout(60, TimeUnit.SECONDS)
//            .readTimeout(60, TimeUnit.SECONDS)
//            .writeTimeout(60, TimeUnit.SECONDS)
//            .addInterceptor(RequestInterceptor())
//            .addInterceptor(loggingInterceptor())

//        return SL.enableTls12OnPreLollipop(client).build()

//        return getUnsafeOkHttpClient(client).enableTls12().build()
        return CustomTrust().client
    }

    private val moshi = Moshi.Builder()
        .add(DefaultIfNullFactory())
        .add(KotlinJsonAdapterFactory())
        .build()

    private fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .client(getOkHTTPClient())
            .baseUrl("https://61e947967bc0550017bc61bf.mockapi.io/api/v1/")
            .addConverterFactory(MoshiConverterFactory.create(moshi)) // Moshi maps JSON to classes
            .addCallAdapterFactory(CoroutineCallAdapterFactory()) // The call adapter handles threads
            .build()
    }

    val apis: APIService by lazy {
        Logger.getLogger(OkHttpClient::class.java.name).level = Level.FINE
        retrofit().create(APIService::class.java)
    }

}

interface APIService {
    @GET("rooms")
    suspend fun getRoomsData(): Response<List<Room>>

    @GET("people")
    suspend fun getEmployeesData(): Response<List<Employee>>
}
