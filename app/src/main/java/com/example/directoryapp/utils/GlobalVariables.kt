package com.example.directoryapp.utils

import androidx.lifecycle.MutableLiveData

object GlobalVariables {
    val showLoading: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>(false) }
    val snackBarMessage: MutableLiveData<String> by lazy { MutableLiveData<String>() }
}