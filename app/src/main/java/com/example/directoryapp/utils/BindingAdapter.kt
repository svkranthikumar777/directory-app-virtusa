package com.example.directoryapp.utils

import android.text.Html
import android.text.TextUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.example.directoryapp.R

@androidx.databinding.BindingAdapter("imageUrl")
fun loadImageWithGlide(imageView: ImageView?, imageUrl: String?) {
    if (imageView == null || TextUtils.isEmpty(imageUrl)) return;

    try {
        Glide.with(imageView.context)
            .load(imageUrl)
            .circleCrop()
            .placeholder(R.drawable.ic_person_placeholder)
            .error(R.drawable.ic_person_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(imageView);
    } catch (e: Exception) {
        e.printStackTrace();
    }
}

@androidx.databinding.BindingAdapter("date")
fun parseDate(textView: TextView?, date: String?) {
    if (textView == null || TextUtils.isEmpty(date)) return;

    try {
        setHtmlText(textView, "",
            textView.context.resources.getString(R.string.created_date, Utility.parseDateTime(date, Utility.FORMAT_RESPONSE, Utility.FORMAT_DISPLAY)),
        )
    } catch (e: Exception) {
        e.printStackTrace();
    }
}


@BindingAdapter(value = ["htmlText", "placeHolder"], requireAll = false)
fun setHtmlText(textView: TextView?, placeHolder: String?, value: String?) {
    if (textView == null) return

    try {
        textView.text = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Html.fromHtml(value ?: placeHolder ?: "", HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else Html.fromHtml(value ?: placeHolder ?: "")
    } catch (e: Exception) {
        e.printStackTrace()
    }
}
