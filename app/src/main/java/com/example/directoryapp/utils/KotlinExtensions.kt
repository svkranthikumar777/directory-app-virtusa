package com.example.directoryapp.utils

import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

fun String?.isValid(): Boolean = this != null && !this.equals("null", true) && this.trim().isNotBlank()
fun Int?.isValid(): Boolean = this != null
fun Boolean?.isValid(): Boolean = this != null

fun View?.showView() {
    if (this == null) return

    visibility = View.VISIBLE
}

fun View?.hideView(removeView: Boolean?) {
    if (this == null) return

    visibility = if (removeView.isValid() && removeView == true) View.GONE else View.INVISIBLE
}

fun Context?.showToast(msg: String?) {
    if (this == null || msg == null) return

    try {
        Handler(Looper.getMainLooper()).post {
            val toast = Toast.makeText(this, msg, Toast.LENGTH_LONG)

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q) {
                toast.setGravity(Gravity.CENTER, 0, 0)
            }

            toast.show()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun showSnackBarMessage(message: String, view: View, f: (Snackbar.() -> Unit)?) {
    try {
        val snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
        f?.let { snack.it() }
        snack.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}