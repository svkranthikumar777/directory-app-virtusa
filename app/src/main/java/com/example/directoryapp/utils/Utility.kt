package com.example.directoryapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Point
import android.os.Build
import android.view.Display
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.directoryapp.MainApplication
import java.text.SimpleDateFormat
import java.util.*


object Utility {
    const val FORMAT_RESPONSE = "yyyy-MM-dd'T'hh:mm:ss.SSSSSS'Z'"
    const val FORMAT_DISPLAY = "dd MMM yyyy HH:mm"

    fun isValidContext(context: Context?): Boolean {
        if (context == null) return false
        if (context is AppCompatActivity) if (context.isDestroyed || context.isFinishing) return false

        return true
    }

    fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    fun parseDateTime(dateString: String?, originalFormat: String, outputFromat: String): String {
        if (!dateString.isValid() || !originalFormat.isValid() || !outputFromat.isValid()) return ""
        var formattedDate = ""

        try {
            val originalFormatter = SimpleDateFormat(originalFormat, Locale.ENGLISH)
            val outputFormatter = SimpleDateFormat(outputFromat, Locale.ENGLISH)
            val date: Date? = originalFormatter.parse(dateString!!)
            if (date != null) {
                formattedDate = outputFormatter.format(date)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return formattedDate
    }

    fun expand(v: View?) {
        if (v == null) return

        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val targetHeight: Int = v.measuredHeight

        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                v.layoutParams.height = if (interpolatedTime == 1f) LinearLayout.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        a.duration = (((targetHeight / v.context.resources.displayMetrics.density) * 1.4).toLong())
        v.startAnimation(a)
    }

    fun collapse(v: View?, callback: (result: Boolean?) -> Unit) {
        if (v == null) return

        val initialHeight: Int = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        a.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                callback.invoke(true)
            }

            override fun onAnimationRepeat(p0: Animation?) {
            }
        })
        a.duration = (((initialHeight / v.context.resources.displayMetrics.density) * 1.4).toLong())
        v.startAnimation(a)
    }

}