package com.example.directoryapp.ui

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.directoryapp.R
import com.example.directoryapp.databinding.ItemEmployeeBinding
import com.example.directoryapp.databinding.ItemRoomBinding
import com.example.directoryapp.model.BaseModel
import com.example.directoryapp.model.Employee
import com.example.directoryapp.model.Room

enum class TYPE {
    ROOM, EMPLOYEE
}

class CommonAdapter(private var type: TYPE) : RecyclerView.Adapter<CommonAdapter.ItemViewHolder>() {
    private var items: List<BaseModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemBinding: ViewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            if (viewType == TYPE.EMPLOYEE.ordinal) R.layout.item_employee else R.layout.item_room,
            parent, false
        )

        return ItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items?.get(position))
    }

    inner class ItemViewHolder(private val itemBinding: ViewBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(data: BaseModel?) {
            itemView.setOnClickListener {
                try {
                    val bundle = Bundle()
                    bundle.putSerializable("data", data)

                    //TODO display item detail page if required
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            when (itemBinding) {
                is ItemEmployeeBinding -> {
                    itemBinding.data = data as Employee
                }
                is ItemRoomBinding -> {
                    itemBinding.data = data as Room
                    itemBinding.cvContent.setCardBackgroundColor(Color.parseColor(if (data.isOccupied == true) "#ffdfdf" else "#F5FBEC"))
                }
                else -> {}
            }
        }
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return type.ordinal
    }

    fun setItems(newData: List<BaseModel>?) {
        this.items = newData
    }
}