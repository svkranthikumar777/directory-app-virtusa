package com.example.directoryapp.ui.activities

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.directoryapp.R
import com.example.directoryapp.databinding.ActivityMainBinding
import com.example.directoryapp.ui.fragments.EmployeesFragment
import com.example.directoryapp.ui.fragments.RoomsFragment
import com.example.directoryapp.utils.GlobalVariables
import com.example.directoryapp.utils.Utility
import com.example.directoryapp.utils.isValid
import com.example.directoryapp.utils.showSnackBarMessage
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.*
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityMainBinding
    private var titles = arrayOf("")
    private var desc = arrayOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(mBinding.toolbar)

        GlobalVariables.snackBarMessage.observe(this, {
            if (it.isValid()) {
                GlobalVariables.snackBarMessage.value = null
                showSnackBarMessage(it, findViewById(android.R.id.content), null)
            }
        })

        mBinding.searchView.setOnSearchClickListener {
            mBinding.searchView.setBackgroundResource(R.drawable.bg_searchview)
        }

        mBinding.searchView.setOnCloseListener {
            mBinding.searchView.setQuery("", true)
            mBinding.searchView.clearFocus()
            mBinding.searchView.background = null
            return@setOnCloseListener false
        }

        mBinding.searchView.onActionViewCollapsed()

        mBinding.tvTitle.setOnClickListener {
            if (mBinding.tabs.visibility == View.GONE)
                Utility.expand(mBinding.tabs)
            else Utility.collapse(mBinding.tabs) {}
        }

        titles = resources.getStringArray(R.array.titles)
        desc = resources.getStringArray(R.array.titles_desc)

        initViewPager()
        initTabLayout()
    }

    private fun initViewPager() {
        mBinding.viewPager.apply {
            adapter = ViewPagerAdapter(this@MainActivity)
            isUserInputEnabled = false
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    mBinding.tvTitle.text = titles[position]
                }
            })
        }
    }

    private fun initTabLayout() {
        for (s: String in titles) {
            mBinding.tabs.addTab(mBinding.tabs.newTab().setText(s))
        }

        if (mBinding.tabs.tabCount > 1) {
            val tabs = mBinding.tabs.getChildAt(0)
            if (tabs is LinearLayout) {
//                tabs.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
//                val drawable = GradientDrawable()
//                drawable.setColor(ContextCompat.getColor(this, R.color.shimmer_placeholder))
//                drawable.setSize(2, 1)
//                tabs.dividerPadding = 10
//                tabs.dividerDrawable = drawable

                tabs.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
                val size = tabs.measuredWidth
                if (size > Utility.getScreenWidth()) {
                    mBinding.tabs.tabMode = MODE_SCROLLABLE
                    mBinding.tabs.tabGravity = GRAVITY_CENTER
                } else {
                    mBinding.tabs.tabMode = MODE_FIXED
                    mBinding.tabs.tabGravity = GRAVITY_FILL
                }
            }

            if (tabs is ViewGroup)
                for (i in 0 until tabs.childCount) {
                    val tab = tabs.getChildAt(i)
                    val p = tab.layoutParams as ViewGroup.MarginLayoutParams

                    when (i) {
                        0 -> p.setMargins(resources.getDimension(R.dimen._25sdp).toInt(), 0, resources.getDimension(R.dimen._10sdp).toInt(), 0)
                        tabs.childCount - 1 -> p.setMargins(resources.getDimension(R.dimen._10sdp).toInt(), 0, resources.getDimension(R.dimen._25sdp).toInt(), 0)
                        else -> p.setMargins(resources.getDimension(R.dimen._10sdp).toInt(), 0, resources.getDimension(R.dimen._10sdp).toInt(), 0)
                    }

                    tab.requestLayout()
                }
        }

//        TabLayoutMediator(mBinding.tabs, mBinding.viewPager) { tab, position ->
//            tab.text = titles[position]
//        }.attach()

        mBinding.tabs.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: Tab?) {
                Utility.collapse(mBinding.tabs) {
                    tab?.run {
                        mBinding.tvTitle.contentDescription = desc[position]

                        CoroutineScope(Dispatchers.IO).launch {
                            delay(100)
                            withContext(Dispatchers.Main) {
                                mBinding.viewPager.currentItem = position
                            }
                        }
                    }
                }
            }

            override fun onTabUnselected(tab: Tab?) {
            }

            override fun onTabReselected(tab: Tab?) {
            }
        })

        mBinding.tabs.getTabAt(0)?.select()

        if (mBinding.tabs.tabCount <= 1) {
            mBinding.tvTitle.setCompoundDrawables(null, null, null, null)
            mBinding.tvTitle.setOnClickListener(null)
        }
    }

    override fun onBackPressed() {
        if (mBinding.viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            mBinding.viewPager.currentItem = mBinding.viewPager.currentItem - 1
        }
    }

    private inner class ViewPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        private val fragments = arrayOf(EmployeesFragment(), RoomsFragment())

        override fun getItemCount(): Int = fragments.size
        override fun createFragment(position: Int): Fragment = fragments[position]
    }

}