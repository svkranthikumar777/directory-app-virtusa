package com.example.directoryapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.directoryapp.model.Employee
import com.example.directoryapp.model.Room
import com.example.directoryapp.retrofit.ErrorResponseHandler
import com.example.directoryapp.retrofit.RetrofitApiService
import com.example.directoryapp.utils.GlobalVariables
import com.example.directoryapp.utils.isValid
import kotlinx.coroutines.launch

class DirectoryViewModel : ViewModel() {
    internal var loading = MutableLiveData(true)

    internal var roomsData = MutableLiveData<List<Room>>()
    internal var employeesData = MutableLiveData<List<Employee>>()

    internal fun getRoomsData() {
        loading.value = true

        viewModelScope.launch {
            try {
                val response = RetrofitApiService.apis.getRoomsData()

                loading.value = false
                if (response.isSuccessful) {
                    val body: List<Room>? = response.body()

                    body?.let {
                        roomsData.postValue(it)
                        return@launch
                    }
                }

                GlobalVariables.snackBarMessage.postValue("Unable to fetch the data")
            } catch (e: Exception) {
                ErrorResponseHandler.parseException(e)
                loading.value = false
            }
        }
    }

    internal fun getEmployeesData() {
        loading.value = true

        viewModelScope.launch {
            try {
                val response = RetrofitApiService.apis.getEmployeesData()
                loading.value = false

                if (response.isSuccessful) {
                    val body: List<Employee>? = response.body()

                    body?.let {
                        employeesData.postValue(it)
                        return@launch
                    }
                }

                GlobalVariables.snackBarMessage.postValue("Unable to fetch the data")
            } catch (e: Exception) {
                ErrorResponseHandler.parseException(e)
                loading.value = false
            }
        }
    }

    fun filterPeople(searchKeyword: String?): List<Employee>? {
        if (!searchKeyword.isValid()) return employeesData.value

        val temp: MutableList<Employee> = mutableListOf()

        try {
            employeesData.value?.let {
                temp.addAll(it.filter { s ->
                    s.firstName?.lowercase()!!.contains(searchKeyword!!.lowercase())
                            || s.lastName?.lowercase()!!.contains(searchKeyword.lowercase())
                            || s.favouriteColor?.lowercase()!!.contains(searchKeyword.lowercase())
                }.toList())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return temp
    }


}