package com.example.directoryapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.directoryapp.R
import com.example.directoryapp.databinding.FragmentRoomsBinding
import com.example.directoryapp.ui.CommonAdapter
import com.example.directoryapp.ui.DirectoryViewModel
import com.example.directoryapp.ui.TYPE
import com.example.directoryapp.utils.hideView
import com.example.directoryapp.utils.showView

class RoomsFragment : Fragment() {
    private lateinit var mBinding: FragmentRoomsBinding
    private lateinit var mViewModel: DirectoryViewModel
    private lateinit var mAdapter: CommonAdapter
    private lateinit var mAdapter1: CommonAdapter

    private var isInitMode = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            actionBar?.title = resources.getString(R.string.rooms)
            this.findViewById<View>(R.id.searchView).hideView(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewModel = ViewModelProvider(this, defaultViewModelProviderFactory)[DirectoryViewModel::class.java]
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_rooms, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.swipeRefreshLayout.setOnRefreshListener {
            mBinding.swipeRefreshLayout.isRefreshing = false
            getRoomsData()
        }

        initViews()
        setUpObservers()
    }

    override fun onResume() {
        super.onResume()

        try {
            activity?.findViewById<View>(R.id.searchView).hideView(true)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (isInitMode) getRoomsData()
        isInitMode = false
    }

    private fun initViews() {
        mBinding.recyclerview.apply {
            layoutManager = GridLayoutManager(context, 1)
            adapter = CommonAdapter(TYPE.ROOM).apply { mAdapter = this }
        }

        mBinding.recyclerview1.apply {
            layoutManager = GridLayoutManager(context, 1)
            adapter = CommonAdapter(TYPE.ROOM).apply { mAdapter1 = this }
        }
    }

    private fun setUpObservers() {
        mViewModel.roomsData.observe(viewLifecycleOwner, {
            it?.let { items ->
                items.filter { room -> room.isOccupied == false }.toList().run {
                    mAdapter.setItems(this)
                    mBinding.tvAvailable.text = activity?.resources?.getString(R.string.available).plus(" ($size)")
                }

                items.filter { room -> room.isOccupied == true }.toList().run {
                    mAdapter1.setItems(this)
                    mBinding.tvOccupied.text = activity?.resources?.getString(R.string.occupied).plus(" ($size)")
                }
            } ?: run {
                mAdapter.setItems(null)
                mBinding.tvEmptyData.showView()
                mBinding.recyclerview.hideView(true)

                mBinding.tvAvailable.text = activity?.resources?.getString(R.string.available)
                mBinding.tvOccupied.text = activity?.resources?.getString(R.string.occupied)
            }
        })

        mViewModel.loading.observe(viewLifecycleOwner, {
            if (it) showShimmerLoading() else hideShimmerLoading()
        })
    }

    private fun getRoomsData() {
        mViewModel.getRoomsData()
    }

    private fun showShimmerLoading() {
        mBinding.tvEmptyData.hideView(true)

        mBinding.recyclerview.hideView(true)
        mBinding.shimmerView.showView()
        mBinding.shimmerView.startShimmer()

        mBinding.recyclerview1.hideView(true)
        mBinding.shimmerView1.showView()
        mBinding.shimmerView1.startShimmer()
    }

    private fun hideShimmerLoading() {
        mBinding.recyclerview.showView()
        mBinding.shimmerView.hideView(true)
        mBinding.shimmerView.stopShimmer()

        mBinding.recyclerview1.showView()
        mBinding.shimmerView1.hideView(true)
        mBinding.shimmerView1.stopShimmer()
    }
}