package com.example.directoryapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.directoryapp.R
import com.example.directoryapp.databinding.FragmentEmployeesBinding
import com.example.directoryapp.model.Employee
import com.example.directoryapp.ui.CommonAdapter
import com.example.directoryapp.ui.DirectoryViewModel
import com.example.directoryapp.ui.TYPE
import com.example.directoryapp.utils.hideView
import com.example.directoryapp.utils.showView

class EmployeesFragment : Fragment() {
    private lateinit var mBinding: FragmentEmployeesBinding
    private lateinit var mViewModel: DirectoryViewModel
    private lateinit var mAdapter: CommonAdapter

    private var isInitMode = true
    private var mSearchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            actionBar?.title = resources.getString(R.string.employees)
            this.findViewById<View>(R.id.searchView).showView()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewModel = ViewModelProvider(this, defaultViewModelProviderFactory)[DirectoryViewModel::class.java]
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_employees, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.swipeRefreshLayout.setOnRefreshListener {
            mBinding.swipeRefreshLayout.isRefreshing = false
            getEmployeesData()
        }

        initViews()
        setUpObservers()
    }

    override fun onResume() {
        super.onResume()

        try {
            activity?.findViewById<View>(R.id.searchView).showView()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (isInitMode) getEmployeesData()
        isInitMode = false
    }

    private fun initViews() {
        mBinding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = CommonAdapter(TYPE.EMPLOYEE).apply { mAdapter = this }
        }

        try {
            mSearchView = activity?.findViewById<View>(R.id.searchView) as SearchView
            mSearchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.isNotBlank().apply {
                        val filteredList = mViewModel.filterPeople(newText)
                        setItemsToAapter(filteredList)
                        mAdapter.notifyDataSetChanged()
                    } ?: run {
                        setItemsToAapter(mViewModel.employeesData.value)
                        mAdapter.notifyDataSetChanged()
                    }

                    return true
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setUpObservers() {
        mViewModel.employeesData.observe(viewLifecycleOwner, {
            setItemsToAapter(it)
        })

        mViewModel.loading.observe(viewLifecycleOwner, {
            if (it) showShimmerLoading() else hideShimmerLoading()
        })
    }

    private fun setItemsToAapter(data: List<Employee>?) {
        data?.let { items ->
            mAdapter.setItems(items)
            mAdapter.notifyDataSetChanged()
            mSearchView?.showView()

            if (items.isEmpty()) {
                mBinding.recyclerview.hideView(true)
                mBinding.tvEmptyData.showView()
            } else {
                mBinding.recyclerview.showView()
                mBinding.tvEmptyData.hideView(true)
            }
        } ?: run {
            mAdapter.setItems(null)
            mBinding.tvEmptyData.showView()
            mBinding.recyclerview.hideView(true)
        }
    }

    private fun getEmployeesData() {
        mSearchView?.run {
            setQuery("",false)
            clearFocus()
            hideView(true)
        }

        mViewModel.getEmployeesData()
    }

    private fun showShimmerLoading() {
        mBinding.tvEmptyData.hideView(true)
        mBinding.recyclerview.hideView(true)
        mBinding.shimmerView.showView()
        mBinding.shimmerView.startShimmer()
    }

    private fun hideShimmerLoading() {
        mBinding.recyclerview.showView()
        mBinding.tvEmptyData.hideView(true)
        mBinding.shimmerView.hideView(true)
        mBinding.shimmerView.stopShimmer()
    }
}